## Install Ansible
```
sudo apt install ansible
ansible-galaxy collection install community.general
ansible-galaxy collection install --force community.docker
ansible-galaxy install --force dmotte.disable_ipv6
ansible-galaxy install --force djarbz.proxmox_subscription_nag
ansible-galaxy install --force geerlingguy.swap
ansible-galaxy install --force geerlingguy.security
ansible-galaxy install --force alf149.crowdsec
```

### Validate the install
```
ansible --version
```
## Decrypt Hosts File
```
ansible-vault decrypt --ask-vault-password hosts.yml
```

## Run Playbooks
```
ansible-playbook -i ./hosts.yml ./playbooks/<playbook>
```
### Run a playbook limiting it to one host
```
ansible-playbook -i ./hosts.yml --limit proxmox2 ./playbooks/proxmox_setup.yml
```
### Run playbook to install ssh keys first time
```
ansible-playbook -i ./hosts.yml --limit proxmox1 --ask-pass ./playbooks/proxmox_setup.yml
```

### Run playbook to redeploy containers on a host
```
ansible-playbook -i ./hosts.yml --limit chugger ./playbooks/docker_services.yml 
```


### Run playbook to stop containers on a host
```
ansible-playbook -i ./hosts.yml --limit chugger ./playbooks/docker_services.yml --tags containers_stop
```

### Run playbook to stop containers on a host
```
ansible-playbook -i ./hosts.yml --limit chugger ./playbooks/docker_services.yml --tags containers_restart
```

### Run playbook to do initial ansible_user setup
```
ansible-playbook -i 192.168.3.118, -u ubuntu --ask-pass --ask-become-pass ./playbooks/create_ansible_user.yml
ansible-playbook -i hosts.yml --limit pihole1 ./playbooks/create_ansible_user.yml
```

## Upgrade packages Playbook
```
ansible-playbook -i ./hosts.yml ./playbooks/upgrades.yml
ansible-playbook --limit '!pihole1' ./playbooks/upgrades.yml
```

## echo all variables for a host
``` 
ansible <host pattern> -m debug -a "var=hostvars[inventory_hostname]"
```