---
- name: Setup Proxmox ubuntu guest template
  # This needs to be a proxmox host, since some of the commands depend on `qm`.
  hosts: proxmox

  #vars_files:
  # Secrets contains a variable `proxmox_api_secret`. See https://pve.proxmox.com/pve-docs/chapter-pveum.html#pveum_tokens for generating a token.
  #- ../../vars/secrets.yml

  vars:
  - cloud_init_ssh_keys_path: /tmp/cloud_init_ssh_keys
  - cloud_init_user: ci

  # Force download of the ISO. ONce this happens once, you probably don't need to do it *every* time.
  - force_download: false
  # URL of the image to download. 
  - iso_source_url: "https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.img"
  # Where to save the ISO.
  - iso_target_path: "./focal-server-cloudimg-amd64.img"
  # Some calls need to know the storage 'name'. Specify that here.
  - storage_target: "local-lvm"
  # A really, REALLY rudementary way to define multiple templates. We could probably do this later, but.  eh.
  # pvesh get /cluster/resources --type vm --output-format text --noborder --noheader | awk '{print $NF}' | sort -n | tail -n 1 | awk '{print $1 + 1}'
  # - vm_defs:
  #     - id: 8002
  #       name: "ubuntu-cloud"
  #       nets:
  #         - id: 0
  #           bridge: "vmbr0"
  - vm_name: "ubuntu-cloud-template"

  tasks:
  # PIP is required to install the proxmoxer library, needed by community.general ansible collection.

    - name: Install packages
      ansible.builtin.apt:
        name:
          - python3-pip
          - libguestfs-tools
          - python3-proxmoxer
        state: latest
        update_cache: true

    - name: Download cloud image
      ansible.builtin.get_url:
        url: "{{ iso_source_url }}"
        dest: "{{ iso_target_path }}"
        force: "{{ force_download }}"
        mode: 0777

    - name: install guest tools
      ansible.builtin.shell:
        cmd: "virt-customize -a {{ iso_target_path }}  --install qemu-guest-agent"

    - name: find the next available vm id
      shell: "pvesh get /cluster/resources --type vm --output-format text --noborder --noheader | awk '{print $NF}' | sort -n | tail -n 1 | awk '{print $1 + 1}'"
      register: pvesh_output

    - name: set next_available_vmid to {{ pvesh_output.stdout }}
      set_fact:
        next_available_vmid: "{{ pvesh_output.stdout }}"


    # - name: find the next available vm id
    #   command: "pvesh get /cluster/resources --type vm --output-format text --noborder --noheader | awk '{print $NF}' | sort -n | tail -n 1 | awk '{print $1 + 1}'"
    #   register: next_available_vmid

    - name: create new virtual machine
      ansible.builtin.shell:
        cmd: "qm create {{ next_available_vmid }} --memory 2048 --core 2 --name {{ vm_name }} --net0 virtio,bridge=vmbr0"

    - name: Import the cloned image disk
      ansible.builtin.shell:
        cmd: "qm importdisk {{ next_available_vmid }} {{ iso_target_path }} {{ storage_target }}"

    - name: Attach the cloned disk.
      ansible.builtin.shell:
        cmd: "qm set {{ next_available_vmid }} --scsihw virtio-scsi-pci --scsi0 {{ storage_target }}:vm-{{ next_available_vmid }}-disk-0"

    - name: Add the CloudInit disk
      ansible.builtin.shell:
        cmd: "qm set {{ next_available_vmid }} --ide2 {{ storage_target }}:cloudinit"

    - name: Make the cloud init drive bootable and restrict BIOS to boot from disk only
      ansible.builtin.shell:
        cmd: "qm set {{ next_available_vmid }} --boot c --bootdisk scsi0"

    - name: add a serial console
      ansible.builtin.shell:
        cmd: "qm set {{ next_available_vmid }} --serial0 socket --vga serial0"

    - name: enable guest agent
      ansible.builtin.shell:
        cmd: "qm set {{ next_available_vmid }} --agent enabled=1"

    - name: Copy SSh Key to temp location
      authorized_key:
         user: root
         state: present
         path: "{{ cloud_init_ssh_keys_path }}"
         key: "{{ lookup('file', item) }}"
      with_fileglob:
      - ci_authorized_keys

    - name: add ssh key to 
      ansible.builtin.shell:
        cmd: "qm set {{ next_available_vmid }} --sshkey {{ cloud_init_ssh_keys_path }}"

    - name: change the default user for the ssh key 
      ansible.builtin.shell:
        cmd: "qm set {{ next_available_vmid }} --ciuser {{ cloud_init_user }}"

    - name: network to dhcp # or --ipconfig0 ip=10.10.10.222/24,gw=10.10.10.1
      ansible.builtin.shell:
        cmd: "qm set {{ next_available_vmid }} --ipconfig0 ip=dhcp"

    # to check the created cloud init
    # qm cloudinit dump 8000 user

    - name: convert to template
      ansible.builtin.shell:
        cmd: "qm template {{ next_available_vmid }}"

    - name: Remove file downloaded cloud image
      ansible.builtin.file:
        path: "{{ iso_target_path }}"
        state: absent

    # - name: debug test
    #   ansible.builtin.debug:
    #     msg: "{{ next_available_vmid }}"
    #   loop: "{{ vm_defs }}"