#!/bin/bash

# Set threshold percentage (e.g., 90 means 90%)
THRESHOLD=85

# Email details
HOSTNAME=$(hostname)
RECIPIENT="root"
SUBJECT="Disk Space Alert on Proxmox Host $HOSTNAME"
MAIL_CMD="/usr/bin/mail"

# Initialize an empty string for the email content
EMAIL_CONTENT=""


# Check disk usage for all mounted filesystems except 'tmpfs' and '/dev/fuse' and udev
while read -r line; do
    FILESYSTEM=$(echo $line | awk '{print $1}')
    USAGE=$(echo $line | awk '{print $5}' | sed 's/%//')

    if [[ $USAGE -gt $THRESHOLD ]]; then
        # Append warning message to the email content
        EMAIL_CONTENT+="Warning: Disk space usage on Filesystem $FILESYSTEM of Proxmox host $HOSTNAME has exceeded ${THRESHOLD}%.\nCurrent Usage: ${USAGE}%.\n\n"
    fi
done < <(df | grep -vE 'tmpfs|/dev/fuse|udev')

# If there's any alert content, send the email
if [[ -n "$EMAIL_CONTENT" ]]; then
    echo "sending mail"
    echo "$EMAIL_CONTENT" | $MAIL_CMD -s "$SUBJECT" $RECIPIENT
fi